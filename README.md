# SLAP (Structural variants in Long And Phased genome)

## Introduction
Given a set of phased SNPs (VCF file) and reads aligned to the reference (BAM file), SLAP will assemble the maternal and paternal chromosomes, and give a set of phased structural variants (insertions, deletions and complex).

## Dependencies
### Packages
```
Cython==0.25.2
numpy==1.12.0
pysam==0.10.0
PyVCF==0.6.8
qiime==1.9.1
```
### Repos
```
https://github.com/jmschrei/pomegranate
## Add this version of blasr to FAST_BLASR
https://github.com/mchaisso/blasr
```
### Tools
```
## Add these tools to PATH
BEDTools 
amos 
fastx
samtools 
seqtk
smrtanalysis/3.0.0
canu-1.4
kalign
```
### For minerva users:
```
module load python/2.7.6 py_packages/2.7 java/1.8.0_111 BEDTools amos fastx samtools seqtk
```
## Install
```
git clone https://bitbucket.org/oscarlr/slap.git
cd slap
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python setup.py install

## Testing                                                                                                                                                                                                 
cd test
## for minerva/lab users                                                                                                                                                                                   
sh test_minerva.sh

## for everyone else                                                                                                                                                                                       
sh test.sh
```


## Using SLAP
```
slap [OPTIONS] BAM VCF SAMPLE READS_XML READS_FOFN REF OUTDIR
```
The required input to slap is:

1. `BAM`: Bam file with reads aligned to the reference.
2. `VCF`: Vcf file with phased SNPs.
3. `SAMPLE`: The name of the sample in the VCF file.
4. `READS_XML`: XML file created from dataset tool from smrtanalysis using raw reads
5. `READS_FOFN`: List of raw reads
4. `REF`: Fasta genome reference.
5. `OUTDIR`: Directory where files will be written.

#### Creating XML file
First turn all the bax files to bam files using the bax2bam tool (from smrtanalysis).
Example:
```
bax2bam example.1.bax.h5 example.2.bax.h5 example.3.bax.h5 -o example
```
Then put all the `*.subreads.bam` into a file. (In the above example, one of the files would be example.subreads.bam).
Example:
```
ls /path/to/bams/*.subreads.bam > subreads.fofn
```
Then create the xml using the dataset tool (from smrtanalysis). Example:
```
dataset create --type SubreadSet subreads.xml subreads.fofn 
```
### Options
```
Usage: 
slap [OPTIONS] BAM VCF SAMPLE READS_XML READS_FOFN REF OUTDIR

 SLAP (Structual variants in Long And Phased genome) assembles a diploid
genome using SMRT reads with phased SNPs and calls structural variants.

Options:
  -h, --help            show this help message and exit
  --chrom=CHROM         Chomosomes to assemble in comma seperate format
  --local               Run locally
  --mem_split=MEM_SPLIT
                        Memory for partitioning. (default = 4)
  --threads_split=THREADS_SPLIT
                        Threads for partitioning. (default = 1)
  --jobs_split=JOBS_SPLIT
                        Instead of submitting 1 interval as a job in the
                        cluster, submit this many as a job. (default = 1)
  --walltime_split=WALLTIME_SPLIT
                         Maximum number of time for each partitioning job to
                        spend in the cluster (default = 24)
  --queue_split=QUEUE_SPLIT
                        Queue in the cluster for partitioning (default = low)
  --coverage=COVERAGE   Coverage threshold for assembling region. (default =
                        1)
  --mem_contigs=MEM_CONTIGS
                        Memory for canu/miniasm. Minimum neeed is 8. (default
                        = 8)
  --threads_contigs=THREADS_CONTIGS
                        Threads for canu/miniasm. (default = 1)
  --jobs_contigs=JOBS_CONTIGS
                        Instead of submitting 1 interval as a job in the
                        cluster, submit this many as a job. (default = 50)
  --walltime_contigs=WALLTIME_CONTIGS
                         Maximum number of time for Canu/miniasm to spend in
                        the cluster (default = 24)
  --queue_contigs=QUEUE_CONTIGS
                         Queue in the cluster for Canu/miniasm (default = low)
  --trim_qual=TRIM_QUAL
                        Minimum quality for trimming (default = 30)
  --chemistry=CHEMISTRY
                         Chemistry used to generate PacBio raw data. This can
                        usually be read from the XML file but  sometimes
                        chemistry info is not there.
  --only_split          only split bam files into haplotypes (default = False)
  --only_assemble       only assembly haplotypes (default = False)
  --only_detect_svs     only detect svs (default = False)
  --window=WINDOW       Size of region to assemble (default=250000)
  --min_window=MIN_WINDOW
                        Min size of region to assemble (default=5000) 
```