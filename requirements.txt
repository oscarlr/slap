Cython==0.25.2
h5py==2.6.0
numpy==1.12.0
pysam==0.10.0
PyVCF==0.6.8
qiime==1.9.1
