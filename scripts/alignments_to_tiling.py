import os 
import sys
import pysam
import numpy as np
import argparse
#import bashirlab.utils.seqstats as seqstats
from slap.io.BlasrIO import parseRm5
from slap.io.FastaIO import FastaReader
from slap.model.Range import Range, Ranges
#from bashirlab.model.Sequence import revcomp

def parseArgs( ):
    """Handle command line argument parsing"""    
    parser = argparse.ArgumentParser(description='merged all contigs into a unified set of data via ref comparison.  Writes to hap_10x.fa for the hap_unified file and then to standard out for the denovo patched fasta.  Logging/Stats are printed to standard error.')

    parser.add_argument('m5_fn', type=str,
                       help='m5 file name for hap reads')

    parser.add_argument('--denovo_m5', type=str, default=None,
                       help='m5 file name')

    parser.add_argument('--hap_out_fn', type=str, default="hap_10x.fa",
                       help='output filename for intermediate 10X merged fasta (without denovo)')

    parser.add_argument('--hap_coords_fn', type=str, default="hap_10x.txt",
                       help='output filename for intermediate 10X merged fasta coordinates')

    parser.add_argument('--chr', type=str, default =None,
                        help='chromosome to screen')

    parser.add_argument('--output_fasta', action='store_true', default=False,
                        help='output_fasta')

    parser.add_argument('--center', action='store_true', default=False,
                        help='take center sequence otherwise greedy (not implemented!)')

    parser.add_argument('--aggressive_denovo', action='store_true', default=False,
                        help='aggressively fill in gaps iwth denovo even if sequence is not spanning (NOT IMPLEMENTED) ')

    parser.add_argument('--denovo_coordinates', type=str, default="hap_10x_denovo_coords.txt",
                       help='output filename for de novo coordinates')
    
    parser.add_argument('--ref_fasta', type=str, default=None,
                        help='ref fasta (required with certain options)')

    parser.add_argument('--ref_fill', type=str, default=None,
                        help='fill in gaps with reference sequence ( NOT IMPLEMENTED )')

    parser.add_argument('--denovo_separate', action='store_true',
                        help='split out denovo separatel ')


    args = parser.parse_args()
    return args

def seqToDict ( ref_fn ):
    seqDict = {}
    if ref_fn is not None:
        for entry in FastaReader(ref_fn):
            seqDict [entry.name] = entry.sequence
    return seqDict

def sam_steps (infn):
    
    line1 = "samtools view -Sbh $1 > $1.bam"
    line2 = "samtools sort $1.bam $1.sorted"
    line3 = "samtools index $1.sorted.bam"
    bashout = open ("temp_samtools.sh", 'w')
    print >>bashout, line1
    print >>bashout, line2
    print >>bashout, line3
    bashout.close()
    os.system("sh temp_samtools.sh %s" %(infn))
    return infn+".sorted.bam"

def read_m5_file ( m5fn, chrom=None, primaryOnly=True):
        
    print >>sys.stderr, "reading m5 file ... "
    chr_ahs = []
    seen = set()
    for ah in parseRm5(m5fn, primaryOnly=primaryOnly):
        if chrom is None or ah.target_id == chrom:
            #if ah.query_id in seen:
            #    print >>sys.stderr, "WTF!!!"
            seen.add(ah.query_id)
            chr_ahs.append(ah)
            #if ah.target_start > ah.target_end:
            #    print >>sys.stderr, "target_start is less than target_end"
            ahlen = ah.target_end-ah.target_start
            #if ahlen > 1000000:
            #    print >>sys.stderr, ahlen
    
    chr_ahs.sort(lambda x,y: cmp(x.target_start, y.target_start))
    # do a true sort on the alignment hits
    #chr_ahs.sort()
    print >>sys.stderr, "finished m5 file"
    return chr_ahs


def parseM5 (infn, hap_coords_fn):
    aligned_contigs = read_m5_file (infn)
    aligned_contigs.sort(lambda x,y: cmp(x.target_start, y.target_start))
    contigDict = set()
    containedDict = set()
    ar1 = aligned_contigs[0]
    filtered_contigs = [ar1]
    contigDict.add(ar1.query_id)
    super_contigs = []
    seqsets = []
    seqset = [ar1]
    curr_start = int(ar1.target_start)
    curr_end   = int(ar1.target_end)
    mergelen = 0
    
    
    for ar2 in aligned_contigs[1:]:
        start2 = int(ar2.target_start)
        end2   = int(ar2.target_end)
        if contained_ah(ar1, ar2):
            containedDict.add(ar2.query_id)
            #print >>sys.stderr, "contained!!!"
            continue
        else:
            if ar2.query_id in containedDict:
                print >>sys.stderr, "WTF1"
            if ar2.query_id in contigDict:
                print >>sys.stderr, "WTF2", ar2.query_id

            contigDict.add(ar2.query_id)
            if has_overlap_ah(ar1, ar2):
                curr_end = end2
                mergelen += 1
                seqset.append(ar2)
            else:
                #print >>sys.stderr, "mergelen =", mergelen
                super_contigs.append((curr_start, curr_end))
                seqsets.append(seqset)
                seqset = [ar2]
                curr_start = start2
                curr_end   = end2
                mergelen = 0
            contigDict.add(ar2.query_id)
            ar1 = ar2
    seqsets.append(seqset)
    super_contigs.append((curr_start, curr_end))
    seqs = convert_seq_set_to_str_ah (seqsets, hap_coords_fn)
    #summarize_contigs (super_contigs, seqs, "10X hap")
    return super_contigs, seqs
    

def parseBam(infn):
    
    if infn[-3:] == "sam":
        print >>sys.stderr, "running sam to bam conversion"
        bamfn = sam_steps(infn)
    else:
        bamfn = infn
    bamfile =  pysam.Samfile( bamfn, "rb" )
    
    aligned_contigs = []

    prevah = None
    for ar in bamfile.fetch():
        if ar.is_secondary:
            continue
        aligned_contigs.append(ar)
        # filter out contained contigs and split overlapping contigs
        aligned_contigs.sort(lambda x,y: cmp(x.pos, y.pos))

    contigDict = set()
    containedDict = set()
    ar1 = aligned_contigs[0]
    filtered_contigs = [ar1]
    contigDict.add(ar1.qname)
    super_contigs = []
    seqsets = []
    seqset = [ar1]
    curr_start = int(ar1.pos)
    curr_end   = int(ar1.aend)
    mergelen = 0
    
    
    for ar2 in aligned_contigs[1:]:
        start2 = int(ar2.pos)
        end2   = int(ar2.aend)
        contigDict.add(ar1.qname)

        if contained(ar1, ar2):
            containedDict.add(ar2.qname)
            #print >>sys.stderr, "contained!!!"
            continue
        else:
            if ar2.qname in containedDict:
                print >>sys.stderr, "WTF1"
            if ar2.qname in contigDict:
                print >>sys.stderr, "WTF2"

            contigDict.add(ar2.qname)
            if has_overlap(ar1, ar2):
                curr_end = end2
                mergelen += 1
                seqset.append(ar2)
            else:
                #print >>sys.stderr, "mergelen =", mergelen
                super_contigs.append((curr_start, curr_end))
                seqsets.append(seqset)
                seqset = [ar2]
                curr_start = start2
                curr_end   = end2
                mergelen = 0
            ar1 = ar2
    seqsets.append(seqset)
    super_contigs.append((curr_start, curr_end))
    seqs = convert_seq_set_to_str (seqsets)
    #summarize_contigs (super_contigs, seqs, "10X hap")
    return super_contigs, seqs

def summarize_contigs (super_contigs, seqs, prefix):

    print >>sys.stderr, prefix
    print >>sys.stderr, len(seqs), len(super_contigs)

    # print super contig 
    total_size = 0
    contig_lens = []
    for sc in super_contigs:
        curr_start, curr_end = sc
        #print curr_start, curr_end, (curr_end-curr_start)
        total_size += (curr_end-curr_start)
        contig_lens.append((curr_end-curr_start))
    
    print >>sys.stderr, "contig intervals: ", total_size, float(total_size)/1000000, seqstats.calculateN50(contig_lens)

    total_size = 0
    contig_lens = []
    for seq in seqs:
        n = len(seq)
        total_size += n
        contig_lens.append(n)
    
    print >>sys.stderr, "contig seqs: ", total_size, float(total_size)/1000000, seqstats.calculateN50(contig_lens)
    print >>sys.stderr, "*"*50
    sys.stderr.flush()
    
    
def get_pos_in_ar (aps, ref_pos):
    print >>sys.stderr, aps
    return_flag = False
    for ap in aps:
        #print >>sys.stderr, ap
        if return_flag or (ap[1] is not None and ap[1] == ref_pos):
            #print >>sys.stderr, "yo!"
            if ap[0] is not None:
                return ap[0]
            else:
                return_flag = True
    print >>sys.stderr, "didn't find a match!!!!"

def convert_seq_set_to_str (seqsets, method = "greedy"):
    seqs = []
    for seqset in seqsets:
        seqlist = []
        ar = seqset[0]
        seqlist.append(ar.seq[ar.qstart:ar.qend])
        curr_target_end = ar.aend
        if len(seqset) > 2:
            for ar in seqset[1:-1]:
                #print >>sys.stderr, ar.aligned_pairs
                start = get_pos_in_ar(ar.aligned_pairs, curr_target_end)
                #print >>sys.stderr, "start:", start
                #print >>sys.stderr, "curr_target_end:", curr_target_end
                seqlist.append(ar.seq[ar.qstart:ar.qend][start:])
                curr_target_end = ar.aend
        if len(seqset) > 1:
            ar = seqset[-1]
            #print >>sys.stderr, ar.aligned_pairs
            start = get_pos_in_ar(ar.aligned_pairs, curr_target_end)
            #print >>sys.stderr, "start:", start
            #print >>sys.stderr, "curr_target_end:", curr_target_end
            seqlist.append(ar.seq[ar.qstart:ar.qend][start:])
        seqs.append("".join(seqlist))
    return seqs


def convert_seq_set_to_str_ah (seqsets, hap_coords_fn, method = "greedy"):
    hap_coords_out = open (hap_coords_fn, 'w')    
    seqs = []
    for seqset in seqsets:
        seqlist = []
        ar = seqset[0]
        ar._convertCoordinates()
        seqlist.append(ar.qseq)
        curr_target_end = ar.target_end 
        print >>hap_coords_out, ar.query_id, ar.query_start, ar.query_end, ar.target_id, ar.target_start, ar.target_end
        if len(seqset) > 2:
            for ar in seqset[1:-1]:
                arseq, qid, qstart, qend = get_alignmenthit_subseq(ar, curr_target_end, ar.target_end-1, details=True)
                seqlist.append(arseq)
                curr_target_end = ar.target_end
                print >>hap_coords_out, qid, qstart, qend, ar.target_id, curr_target_end, ar.target_end
        if len(seqset) > 1:
            ar = seqset[-1]
            arseq, qid, qstart, qend = get_alignmenthit_subseq(ar, curr_target_end, ar.target_end-1, details=True)
            seqlist.append(arseq)
            print >>hap_coords_out, qid, qstart, qend, ar.target_id, curr_target_end, ar.target_end
        seqs.append("".join(seqlist))
    hap_coords_out.close()
    return seqs

def get_alignmenthit_subseq (ah, ref_start, ref_end, details=False):
    invertflag = False
    if ah.target_strand == 1:
        invertflag = True

    rev_ref_start = ref_start - ah.target_start
    rev_ref_end = ref_end - ah.target_start
    #if invertflag:
        #print >>sys.stderr, "inverting_pre:", ah.alignedQuery
    ah._convertCoordinates ()
    #if invertflag:
        #print >>sys.stderr, "post:", ah.alignedQuery
    start = ah.tPosToIndex[rev_ref_start]
    end = ah.tPosToIndex[rev_ref_end]
    qstart = ah.qIndexToPos[start]
    qend = ah.qIndexToPos[end]
    seq = ah.qseq[qstart:qend]
    if details:
        return seq, ah.query_id, qstart, qend
    else:
        return seq

def split_out_hap_contigs_with_denovo (hap_contigs, denovo_aligns, hap_seqs, ref_seq):
    g = len(ref_seq)
    print >>sys.stderr, len(hap_contigs), len(hap_seqs), len(denovo_aligns)
    hapRanges = Ranges()
    for hap_contig in hap_contigs: hapRanges.addRange(Range(hap_contig[0], hap_contig[1]))
    
    gap_fill_ranges = []
    hap_denovo_seqs = []
    missing_gaps = []
    hap_index = 0
    seqlist = [hap_seqs[hap_index]]
    total_gap_diff = 0
    print >>sys.stderr, len(list(hapRanges.gaps()))

    # add in a block before and after the first haplotig to indicate missing start and end intervals
    hap_contigs_mod = [[0,0]] # beginning of chrom
    hap_contigs_mod.extend(hap_contigs)
    hap_contigs_mod.append([n,n]) # end of chrom

    out_tups = []
    
    for i in range(1,len(hap_contigs_mod)):
        hap_gap_range = Range(hap_contigs_mod[i-1][1], hap_contigs_mod[i][0])
        hap_index += 1
        gap_filled = False
        spanning_denovo_hits = []
        for denovo_align in denovo_aligns:
            denovo_align_range = Range(denovo_align.target_start, denovo_align.target_end)
            if contained_ranges(denovo_align_range, hap_gap_range):
                denovo_gap_fill_range = Range(hap_gap_range.start-1, hap_gap_range.end+1) 
                gap_fill_ranges.append(denovo_gap_fill_range)
                gap_seq = get_alignmenthit_subseq (denovo_align, hap_gap_range.start, hap_gap_range.end)
                gap_filled = True
                spanning_denovo_hits.append(denovo_align)
                total_gap_diff += len(gap_seq)- len(hap_gap_range)
                seqlist.append(gap_seq)
                seqlist.append(hap_seqs[hap_index])
            elif hap_gap_range.intersets(denovo_align_range):
                pass

    print >>sys.stderr, total_gap_diff
    hap_denovo_seqs.append("".join(seqlist))

    for gfr in gap_fill_ranges:
        hapRanges.addRange(gfr)

    mergedRanges = hapRanges
    mergedContigs = [(r.start, r.end) for r in mergedRanges]

    #summarize_contigs (mergedContigs, hap_denovo_seqs, prefix="10X hap with denovo")
    return mergedContigs, hap_denovo_seqs

def merge_hap_contigs_with_denovo (hap_contigs, denovo_aligns, hap_seqs, allow_partial=False):
    print >>sys.stderr, len(hap_contigs), len(hap_seqs), len(denovo_aligns)
    hapRanges = Ranges()
    for hap_contig in hap_contigs: hapRanges.addRange(Range(hap_contig[0], hap_contig[1]))
    
    gap_fill_ranges = []
    hap_denovo_seqs = []
    missing_gaps = []
    hap_denovo_coords = []
    
    hap_index = 0
    seqlist = [hap_seqs[hap_index]]
    total_gap_diff = 0
    print >>sys.stderr, len(list(hapRanges.gaps()))
    #for hap_gap in hapRanges.gaps():
        #hap_gap_range = Range(hap_gap[0].end, hap_gap[1].start)
    for i in range(1,len(hap_contigs)):
        hap_gap_range = Range(hap_contigs[i-1][1], hap_contigs[i][0])
        hap_index += 1
        gap_filled = False
        spanning_denovo_hits = []
        for denovo_align in denovo_aligns:
            denovo_align_range = Range(denovo_align.target_start, denovo_align.target_end)
            if contained_ranges(denovo_align_range, hap_gap_range):
                denovo_gap_fill_range = Range(hap_gap_range.start-1, hap_gap_range.end+1) 
                gap_fill_ranges.append(denovo_gap_fill_range)
                gap_seq, qid, qstart, qend = get_alignmenthit_subseq (denovo_align, hap_gap_range.start, hap_gap_range.end, details=True)
                gap_filled = True
                spanning_denovo_hits.append(denovo_align)
                total_gap_diff += len(gap_seq)- len(hap_gap_range)
                seqlist.append(gap_seq)
                seqlist.append(hap_seqs[hap_index])
                hap_denovo_coords.append((qid, qstart, qend, denovo_align.target_id, hap_gap_range.start, hap_gap_range.end))
                continue
        # if gap_filled:
        #     spanning_denovo_align = get_best_spanning_hit(spanning_denovo_hits) 
        #         gap_seq = get_alignmenthit_subseq (spanning_denovo_align, hap_gap_range.start, hap_gap_range.end)
        #         total_gap_diff += len(gap_seq)- len(hap_gap_range)
        #         seqlist.append(gap_seq)
        #         seqlist.append(hap_seqs[hap_index])
        #         continue
        if not gap_filled:
            hap_denovo_seqs.append("".join(seqlist))
            seqlist = [hap_seqs[hap_index]]
            missing_gaps.append(hap_gap_range)
    print >>sys.stderr, total_gap_diff
    hap_denovo_seqs.append("".join(seqlist))

    for gfr in gap_fill_ranges:
        hapRanges.addRange(gfr)

    mergedRanges = hapRanges
    mergedContigs = [(r.start, r.end) for r in mergedRanges]

    #summarize_contigs (mergedContigs, hap_denovo_seqs, prefix="10X hap with denovo")
    
    return mergedContigs, hap_denovo_seqs, hap_denovo_coords

def get_max_score (ah1, ah2, ts, te):
    
    # get aligned chunk intervals
    ah1_matchlens = build_aligned_block_size (ah1)
    ah2_matchlens = build_aligned_block_size (ah2)
    
    # indices into the ref indices produced by build_aligned_block_size
    ts1 = ts-ah1.ts
    ts2 = ts-ah2.ts

    te1 = te-ah1.ts
    te2 = te-ah2.ts

    # find optimal score
    interval_len = te-ts
    min_ref_pos = np.zeros(interval_len)
    max_min_val = -1
    max_min_index = -1
    for i in range (interval_len):
        min_ref_pos[i] = min(ah1_matchlens[ts1+i], ah2_matchlens[ts2+i])
        if min_ref_pos[i] > max_min_val:
            max_min_val = min_ref_pos[i]
            max_min_index = i

    # get coordiantes
    ah1._convertCoordinates()
    ah2._convertCoordinates()
    qs1 = ah1.tPosToIndex[ts1+max_min_index]
    qs2 = ah2.tPosToIndex[ts2+max_min_index]
    return qs1, qs2

def build_aligned_block_size (ah):
    n = ah.target_length
    # go forwards
    ref_pos_for = np.zeros(n)
    last_pos = False
    r = 0
    for i in range(len(self.alignedQuery)):
        if self.alignedQuery[i] == self.alignedTarget[i]:
            if last_pos:
                ref_pos_for[r] = ref_pos_for[r-1] +1
            else:
                ref_pos_for[r] = 1
            last_pos = True
            r+=1
        else:
            ref_pos_for[r] = 0
            last_pos = False
            if self.alignedQuery[i] != "-" and self.alignedTarget[i] != "-":
                r+=1
    # go backwards
    last_pos = False
    ref_pos_rev = np.zeros(n)
    r = n-1
    for i in range(len(self.alignedQuery)-1, -1, -1):
        if self.alignedQuery[i] == self.alignedTarget[i]:
            if last_pos:
                ref_pos_rev[r] = ref_pos_rev[r+1] +1
            else:
                ref_pos_rev[r] = 1
            last_pos = True
            r += 1
        else:
            ref_pos_rev[i] = 0
            last_pos = False
            if self.alignedQuery[i] != "-" and self.alignedTarget[i] != "-":
                r+=1
    # get minimal in both directions
    ref_pos = np.zeros(n)
    for r in range(len(self.alignedQuery)):
        ref_pos[r] = min(ref_pos_for[r], ref_pos_rev[r])
    return ref_pos



def find_maximal_overlap (ah1, ah2):
    curr_max = 0
    event_start = 0
    curr_alignment = 0
    

def write_coord_file (hap_denovo_coords, out_fn):
    out = open(out_fn, 'w')
    for hap_denovo_coord in hap_denovo_coords:
        print >>out, "\t".join(map(str, hap_denovo_coord))
    out.close()
       
def contained ( ar1, ar2):
    if ar1.pos < ar2.pos and ar1.aend > ar2.aend:
        return True
    else:
        return False

def contained_tups ( ar1, ar2):
    if ar1[0] < ar2[0] and ar1[1] > ar2[1]:
        return True
    else:
        return False


def contained_ranges ( ar1, ar2):
    if ar1.start < ar2.start and ar1.end > ar2.end:
        return True
    else:
        return False

def has_overlap ( ar1, ar2):
    if ar1.pos < ar2.pos and ar1.aend > ar2.pos and ar1.aend < ar2.aend:
        return True
    else:
        return False

def contained_ah ( ar1, ar2):
    if ar1.target_start <= ar2.target_start and ar1.target_end >= ar2.target_end:
        return True
    else:
        return False


def has_overlap_ah ( ar1, ar2):
    if ar1.target_start <= ar2.target_start and ar1.target_end > ar2.target_start and ar1.target_end < ar2.target_end:
        return True
    else:
        return False
    

def get_score_subalignment ( hit, target_start, target_end):
    m_count, mm_count, ins_count, del_count = hit.get_edits_from_interval(target_start, target_end)
    return m_cuont - mm_coutn, ins_count - del_count

def get_best_spanning_hit(spanning_denovo_hits):
    left_start   = max(map(lambda x: x.target_start, spanning_denovo_hits))
    right_end   = min(map(lambda x: x.target_end, spanning_denovo_hits))
    
    max_core     = 0
    best_hit     = None
    for spanning_denovo_hit in spanning_denovo_hits:
        curr_score = get_score_subalignment (spanning_denovo_hit, left_start, right_end)
        if curr_score > max_score:
            max_score = curr_score
            best_hit = spanning_denovo_hit
    return best_hit

def run():
    args = parseArgs ()
    seqDict = seqToDict(args.ref_fasta)
    #hap_contigs, hap_seqs = parseBam(args.bam_fn)
    hap_contigs, hap_seqs = parseM5(args.m5_fn, args.hap_coords_fn)
    hap_out = open(args.hap_out_fn, 'w')
    for i,seq in enumerate(hap_seqs):
        print >>hap_out, ">seq%i\n%s" %(i, seq)
    hap_out.close()
    if args.denovo_m5:
        # parse denovo alignments in alignment hits
        denovo_aligns = read_m5_file(args.denovo_m5, args.chr)
        # stitch denovo contigs into alignment hits
        mergedContigs, hap_denovo_seqs, hap_denovo_coords = merge_hap_contigs_with_denovo (hap_contigs, denovo_aligns, hap_seqs)
        write_coord_file(hap_denovo_coords, args.denovo_coordinates)
        for i,seq in enumerate(hap_denovo_seqs):
            print ">seq%i\n%s" %(i, seq)
    
run()
