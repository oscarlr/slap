#!/bin/env python
import sys
from slap.io.BlasrIO import rm5LineToHit, parseRm5
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
from os import system
import operator
import numpy as np

def write_sequence(sequences,outdir,reffn):
    for interval,sequence,hap in sequences:
        chrom,start,end = interval
        outfn = "%s/%s_%s.fa" % (outdir,start,end)
        outsequence = []
        if len(sequence) == 1:
            outsequence.append(SeqRecord(Seq(sequence[0]),id=hap))
        else:
            if hap == "h1":
                outsequence.append(SeqRecord(Seq(sequence[0]),id=hap))
                outsequence.append(SeqRecord(Seq(sequence[1]),id="h2"))
            else:
                outsequence.append(SeqRecord(Seq(sequence[0]),id="h1"))
                outsequence.append(SeqRecord(Seq(sequence[1]),id=hap))
        with open(outfn,'w') as f:
            SeqIO.write(outsequence,f,"fasta")
        command = "samtools faidx %s %s:%s-%s >> %s" % (reffn,chrom,int(start)+1,end,outfn)
        system(command)
        
def _get_sequence(query,start,end):
    event_tseq, query_seq, tscoord, tecoord, qscoord, qecoord  = query.getFlanks(start,end,0)
    for i in range(0,3):
        for j in range(0,3):
            event_tseq, query_seq, tscoord, tecoord, qscoord, qecoord  = query.getFlanks(start+i,end-j,0)
            #print i,j,tscoord, tecoord, qscoord, qecoord
            if query_seq != None:
                return query_seq
    return query_seq

def load_intervals(fn):
    clusters = {}
    with open(fn,'r' ) as f:
        for line in f:
            line = line.rstrip().split('\t')
            chrom = line[0]
            start = int(line[1])
            end = int(line[2])
            cluster = line[3]
            if cluster not in clusters:
                clusters[cluster] = []
            clusters[cluster].append([chrom,start,end])
    return clusters

def fix_intervals(clusters):
    intervals = []
    for cluster in clusters:
        chrom = clusters[cluster][0]
        assert len([x[0] for x in clusters[cluster]]) == 1
        if len(clusters[cluster]) == 1:
            intervals.append(clusters[cluster][0])
        else:
            clusters[cluster].sort(key=operator.itemgetter(0,1))
            max_num = max([int(x[2]) for x in clusters[cluster]])
            min_num = min([int(x[1]) for x in clusters[cluster]])
            size_num = max_num-min_num
            x = np.zeros(size_num)
            for chrom, start, end in clusters[cluster]:
                x[int(start)-min_num:int(end)-min_num+1] += 1
            start = None
            end = None
            for i,val in enumerate(x):
                if val == 1 and start == None:
                    start = i
                elif val != 1 and start != None:
                    intervals.append([chrom,start+min_num,end+min_num+1])
                    start = None
                elif val == 1:
                    end = i
            if start != None and end != None:
                intervals.append([chrom,start+min_num-1,end+min_num+1])
    return intervals

def get_intervals(hap_intersections,hap1_only,hap2_only,hap1_m5,hap2_m5,reffn,outdir):
    intervals = {}
    intervals["both"] = fix_intervals(load_intervals(hap_intersections))
    intervals["hap1"] = fix_intervals(load_intervals(hap1_only))
    intervals["hap2"] = fix_intervals(load_intervals(hap2_only))
    return intervals

def main(hap_intersections,hap1_only,hap2_only,hap1_m5,hap2_m5,reffn,outdir):
    intervals = get_intervals(hap_intersections,hap1_only,hap2_only,hap1_m5,hap2_m5,reffn,outdir)
    sequences = []
    for interval in intervals["both"]:
        hap1 = parseRm5(hap1_m5)
        hap2 = parseRm5(hap2_m5)
        chrom,start,end = interval
        for h1 in hap1:
            hap1_sequence = _get_sequence(h1,start,end)
            if hap1_sequence != None:
                break
        for h2 in hap2:
            hap2_sequence = _get_sequence(h2,start,end)
            if hap2_sequence != None:
                break
        print "h1","h2",chrom,start,end
        assert hap1_sequence != None
        assert hap2_sequence != None
        sequences.append([interval,[hap1_sequence,hap2_sequence],"h1"])
    for interval in intervals["hap1"]:
        hap1 = parseRm5(hap1_m5)
        chrom,start,end = interval
        for h1 in hap1:
            hap1_sequence = _get_sequence(h1,start,end)
            if hap1_sequence != None:
                break
        print "h1",chrom,start,end
        assert hap1_sequence != None
        sequences.append([interval,[hap1_sequence],"h1"])        
    for interval in intervals["hap2"]:
        hap2 = parseRm5(hap2_m5)
        chrom,start,end = interval
        for h2 in hap2:
            hap2_sequence = _get_sequence(h2,start,end)
            if hap2_sequence != None:
                break
        print "h2",chrom,start,end
        assert hap2_sequence != None
        sequences.append([interval,[hap2_sequence],"h2"])        
    write_sequence(sequences,outdir,reffn)

if __name__ == '__main__':
    hap_intersections = sys.argv[1]
    hap1_only = sys.argv[2]
    hap2_only = sys.argv[3]
    hap1_m5 = sys.argv[4]
    hap2_m5 = sys.argv[5]
    reffn = sys.argv[6]
    outdir = sys.argv[7]
    main(hap_intersections,hap1_only,hap2_only,hap1_m5,hap2_m5,reffn,outdir)
    
