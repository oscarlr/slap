#!/bin/env python
import sys
import pysam

def save_intervals(intervals,intervals_fn):
    with open(intervals_fn,'w') as f:
        for entry in intervals:
            f.write("%s\t%s\t%s\n" % (entry[0],entry[1],entry[2]))

def bam_intervals(bamfn, cov_min):
    bamfile = pysam.AlignmentFile(bamfn,'rb')
    intervals = []
    start_interval = -1
    end_interval = -1
    prev_end = 0
    for pileupcolumn in bamfile.pileup():
        tid = pileupcolumn.reference_id
        ref_name = bamfile.getrname(tid)
        if pileupcolumn.nsegments >= cov_min: #nsegments instead of n?                                                                                                                                    
            p = pileupcolumn.pos
            if start_interval == -1:
                start_interval = p
            end_interval = p+1
        else:
            if start_interval != -1:
                interval = (ref_name, start_interval, end_interval)
                intervals.append(interval)
                start_interval, end_interval, prev_end = -1, -1, start_interval
    if start_interval != -1:
        interval = (ref_name, start_interval, end_interval)
        intervals.append(interval)
    return intervals

if __name__ == '__main__':
    bamfn = sys.argv[1]
    outbed = sys.argv[2]
    cov = int(sys.argv[3])
    intervals = bam_intervals(bamfn,cov)
    save_intervals(intervals,outbed)
