import os
from slap.io.BlasrIO import parseRm5
from slap.io.FastaIO import FastaReader
#from slap.utils.seqstats import getFastaLengths
import argparse

def parseArgs( ):
    """Handle command line argument parsing"""    
    parser = argparse.ArgumentParser(description='Convert bed to vcf')

    parser.add_argument('fn', type=str,
                       help='m5 file')
    parser.add_argument('target_fn', type=str,
                       help='fasta file to get chrom lengths')
    args = parser.parse_args()
    return args

args = parseArgs()

lengthDict = {}
for entry in FastaReader(args.target_fn):
    lengthDict[entry.name.split()[0]] = len(entry.sequence)


for hit in parseRm5 (args.fn, shift_hit=True, target_len_dict=lengthDict):
    print hit.line



