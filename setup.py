from setuptools import setup, find_packages
from Cython.Build import cythonize
import numpy
include_path = [numpy.get_include()]
setup(
    name='slap',
    url='https://oscarlr@bitbucket.org/oscarlr/slap.git',
    description='Assembling diploid genome and calling structural variants',
    packages=find_packages(),
    include_package_data=True,
    scripts = ["scripts/splitBamByPhasedVcf.py",
               "scripts/coverageBamToBed.py",
               "scripts/alignments_to_tiling.py",
               "scripts/bedM5toOverlapFasta2.py",
               "scripts/msaToVariants.py",
               "scripts/shift_m5_coords.py"],
    entry_points={
        'console_scripts': [ 'slap = slap.slap:main' ]
        },
    ext_modules = cythonize("slap/cython/*.pyx",include_path),
    include_dirs=[numpy.get_include()],
    platforms='any'
)
