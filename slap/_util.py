#!/bin/env python
from os import makedirs, system
from io.FastaIO import FastaReader
from string import Template
from os.path import exists,dirname,isfile
from cluster.lsf import Lsf
import pysam

def create_dir(dirname):
    if not exists(dirname):
        makedirs(dirname)

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def setup_chromosomes(reffn):
    chroms = []
    for record in FastaReader(reffn):
        chroms.append(record.name.split()[0])
    return chroms[:22]

def tmpl_bash(tmplfn,swap,bashfn):
    filein = open("%s/tmpl/%s.sh" % (dirname(__file__),tmplfn))
    src = Template(filein.read())
    output_lines = src.safe_substitute(swap)
    bashfh = open(bashfn,'w')
    bashfh.write(output_lines)
    filein.close()
    bashfh.close()

def submit_bashfile(slap,bashfns,command):
    if len(bashfns) == 0:
        return
    if slap.local:
        for bashfn in bashfns:
            system("sh %s" % bashfn)
        return
    if command == "split":
        mem = slap.mem_split
        n = slap.num_jobs_split
        threads = slap.threads_split
        walltime = slap.walltime_split
        queue = slap.queue_split
    else:
        mem = slap.mem_contigs
        n = slap.num_jobs_contigs
        threads = slap.threads_contigs
        walltime = slap.walltime_contigs
        queue = slap.queue_contigs        
    hpc = Lsf()
    hpc.config(cpu=threads,walltime=walltime,memory=mem,queue=queue)
    if n > 1:
        bashs = chunks(bashfns,n)
        for chunk in bashs:
            outfn = hpc.set_job()
            for bash_fn in chunk:
                hpc.combine_jobs(outfn,bash_fn)
            hpc.submit("%s" % outfn)
    else:
        for bash_fn in bashfns:
            hpc.submit("%s" % bash_fn)
    hpc.wait()
    status,failed_bashes = hpc.status()
    if 2 in status:
        hpc.config(cpu=threads*2,walltime=walltime*2,memory=mem*2,queue=queue)
        if n > 1:
            bashs = chunks(bashfns,n)
            for chunk in bashs:
                outfn = hpc.set_job()
                for bash_fn in chunk:
                    hpc.combine_jobs(outfn,bash_fn)
                hpc.submit("%s" % outfn)
        else:
            for bash_fn in bashfns:
                hpc.submit("%s" % bash_fn)
    hpc.wait()
    status,failed_bashes = hpc.status()
    if 2 in status:
        sys.exit("Your jobs on the cluster failed")




#################
'''
from os import makedirs,system
from string import Template
from os.path import exists,dirname,isfile
from cluster.lsf import Lsf
import sys
import copy
from shutil import copyfile

def create_dir(dirname):
    if not exists(dirname):
        makedirs(dirname)

def tmpl_bash(tmplfn,swap,bashfn):
    copyfile("%s/tmpl/Makefile" % dirname(__file__),"%s/Makefile" % dirname(bashfn))
    filein = open("%s/tmpl/%s.sh" % (dirname(__file__),tmplfn))
    src = Template(filein.read())
    output_lines = src.safe_substitute(swap)
    bashfh = open(bashfn,'w')
    bashfh.write(output_lines)
    filein.close()
    bashfh.close()


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def get_overlap(a, b):
    return max(0, min(a[1], b[1]) - max(a[0], b[0]))

def submit_bashfile(lap,bashfns,job_type,round_):
    if len(bashfns) == 0:
        return
    if lap.local:
        for bashfn in bashfns:
            if isfile("%s" % bashfn):
                system("sh %s" % bashfn)
            else:
                system('%s' % bashfn)
        return
    if job_type == "split":
        mem = lap.mem_split
        n = lap.num_jobs_split
        threads = lap.threads_split
        walltime = lap.walltime_split
        queue = lap.queue_split
    elif job_type == "create_contigs" or job_type == "stitch_contigs": #job_type == "create_contigs":
        mem = lap.mem_create_contigs
        n = lap.num_jobs_create_contigs
        threads = lap.threads_create_contigs
        walltime = lap.walltime_create_contigs
        queue = lap.queue_create_contigs
    else: 
        mem = lap.mem_create_contigs
        n = lap.num_jobs_create_contigs
        threads = lap.threads_create_contigs
        walltime = lap.walltime_create_contigs
        queue = lap.queue_create_contigs
    if job_type == "msa_variants":
        mem = lap.mem_create_contigs
        n = lap.num_jobs_create_contigs
        threads = lap.threads_create_contigs
        walltime = lap.walltime_create_contigs
        queue = lap.queue_create_contigs
    if round_ == 1:
        mem = mem*2
        threads = threads*2
    if round_ == 2:
        mem = mem*3
        threads = threads*3
        walltime = 24
    if round_ == 3:
        sys.exit("ERROR jobs failed in the cluster 3 times")
    hpc = Lsf()
    hpc.config(cpu=threads,walltime=walltime,memory=mem,queue=queue)
    if n > 1:
        bashs = chunks(bashfns,n)
        for chunk in bashs:
            outfn = hpc.set_job()
            for bash_fn in chunk:
                hpc.combine_jobs(outfn,bash_fn)
            hpc.submit("%s" % outfn)
    else:
        for bash_fn in bashfns:
            hpc.submit("%s" % bash_fn)
    hpc.wait()
    #status,failed_bashes = hpc.status()
    # if 2 in status:
    #     submit_bashfile(lap,failed_bashes,job_type,round_=+1)
'''
