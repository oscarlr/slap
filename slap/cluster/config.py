#!/bin/env python
from os import makedirs,environ
from os.path import exists

SCRATCH_DIR="/sc/orga/scratch"
if not exists(SCRATCH_DIR):
    SCRATCH_DIR="/tmp"

OUTDIR="%s/%s/lsf" % (SCRATCH_DIR,environ.get('USER'))
if not exists(OUTDIR):
    makedirs(OUTDIR)

ALLOC_ACCOUNT = environ.get('SJOB_DEFALLOC')
if ALLOC_ACCOUNT == None:
    sys.exit('Initiate default allocation with '
             'SJOB_DEFALLOC environment')
WALLTIME = 24
CPU = 1
MEM = 4
QUEUE = "private"
SLEEPTIME = 30
