import numpy as np
cimport numpy as np

ctypedef np.int_t dtype_ti
#ctypedef np.float_t dtype_tf


#def convert_coordinates_helper (bytes alignedQuery, bytes alignedTarget, np.ndarray[dtype_ti, ndim=1] tPosToIndex, np.ndarray[dtype_ti, ndim=1] qPosToIndex, np.ndarray[dtype_ti, ndim=1] tIndexToPos, np.ndarray[dtype_ti, ndim=1] qIndexToPos):

def convert_coordinates_helper (bytes alignedTarget, bytes alignedQuery, int n, int t_seqlen, int q_seqlen):
    cdef int qPos = 0
    cdef int tPos = 0
    cdef np.ndarray[dtype_ti, ndim=1] tIndexToPos = np.zeros(n, dtype=int)
    cdef np.ndarray[dtype_ti, ndim=1] qIndexToPos = np.zeros(n, dtype=int)

    cdef np.ndarray[dtype_ti, ndim=1] tPosToIndex = np.zeros(t_seqlen, dtype=int)
    cdef np.ndarray[dtype_ti, ndim=1] qPosToIndex = np.zeros(q_seqlen, dtype=int)

    for i in range (n):
        tIndexToPos[i] = tPos
        qIndexToPos[i] = qPos
        tPosToIndex[tPos] = i
        qPosToIndex[qPos] = i
        if alignedQuery[i] != '-':  
            qPos += 1
        if alignedTarget[i] != '-':
            tPos += 1
  
    return tIndexToPos, qIndexToPos, tPosToIndex, qPosToIndex
