import sys
from itertools import *
from slap.model.AlignmentHit import AlignmentHit

def rm5LineToHit (line , terse=False, shift_hit=False, target_len_dict=None):
    """Parses readmatcher -printFormat 5 output into AlignmentHit objects"""
    values = line.rstrip("\n").split()
    hit = AlignmentHit()
    hit.query_id, hit.query_length, hit.query_start, hit.query_end, hit.query_strand = values[0], int(values[1]), int(values[2]), int(values[3]), values[4]

    hit.target_id, hit.target_length, hit.target_start, hit.target_end, hit.target_strand = values[5], int(values[6]), int(values[7]), int(values[8]), values[9]
    hit.score = -1*int(values[10])
    #target_id = values[6] because there is white space in the m5 file before target_id

    hit.query_id = "/".join(hit.query_id.split("/")[0:3])
    
    #print hit.target_strand
    if hit.target_strand == "+":
        hit.target_strand = 0
    else: 
        hit.target_strand = 1

    if hit.query_strand == "+":
        hit.query_strand = 0
    else: 
        hit.query_strand = 1

    if terse:
        return hit

    hit.alignedQuery = values[16]
    hit.aligned = values[17]      
    hit.alignedTarget = values[18]
    
    hit.line = line

    if shift_hit:
        # chr_start_end....
        raw_chrom, raw_coords =  hit.target_id.split(":")
        raw_start, raw_end = raw_coords.split("-")
        raw_start, raw_end = int(raw_start), int(raw_end)
        hit.target_start  += raw_start-1
        hit.target_end    += raw_start-1
        hit.target_id      = raw_chrom
        hit.target_length  = target_len_dict[raw_chrom]        
        # update values
        values[0] = hit.query_id
        values[5] = hit.target_id
        values[6] = hit.target_length
        values[7] = hit.target_start
        values[8] = hit.target_end
        hit.line = " ".join(map(str, values))
    return hit

def parseRm5( filename , terse=False, primaryOnly=False, shift_hit=False, target_len_dict=None):
    """Parses readmatcher -printFormat 5 output into AlignmentHit objects"""
    
    prev_qid = None
    for line in open(filename):
        ah = rm5LineToHit(line, terse, shift_hit, target_len_dict)
        if primaryOnly and ah.query_id == prev_qid:
            continue
        else:
            prev_qid = ah.query_id
            yield ah

def parseRm4( file ):
    """Parses readmatcher -printFormat 4 output into AlignmentHit objects"""

    for line in open(file):
        fields = line.rstrip("\n").split(" ")
        hit = AlignmentHit()
        hit.query_id = fields[0]
        hit.target_id = fields[1]
        hit.score = - int(fields[2])
        hit.pctidentity = float(fields[3])
        hit.query_strand = "+" if fields[4] == "0" else "-"

        hit.query_start =  int(fields[5])
        hit.query_end  =   int(fields[6])
        hit.query_length = int(fields[7])

        # for negative strand readMatcher query coords are reported on reverse 
        # complement of the sequence. For the alignmenthit they need to be 
        # reported on the forward strand
        if hit.query_strand == "-":
            hit.query_end, hit.query_start = hit.query_length - hit.query_start, hit.query_length - hit.query_end 

        hit.target_strand = "+" if fields[8] == "0" else "-"
        hit.target_start  = int(fields[9])
        hit.target_end    = int(fields[10])
        hit.target_length = int(fields[11])
        
        if hit.target_strand == "-":
            hit.target_end, hit.target_start = hit.target_length - hit.target_start, hit.target_length - hit.target_end 
        
        #if hit.target_strand == hit.query_strand: # always report strand for query
        #    hit.target_strand = hit.query_strand = "+"
        #else:
        #    hit.query_strand = "-"
        #    hit.target_strand = "+"

        hit.alignedLength = abs(hit.target_end-hit.target_start)
        yield hit

