from slap.model.Sequence import revcomp as rc
import sys
import numpy as np
import itertools
#import insDelHmm as insdelhmm
#import bashirlab.cython.AlignmentHitCython as ahcython
import AlignmentHitCython as ahcython

class AlignmentHit:
    def __len__( self ):
        return self.query_end - self.query_start
    
    def __init__( self ):
        self.query_length, self.query_id, self.query_start, \
            self.query_end, self.query_strand = 0, None, 0, 0, None
        self.target_length, self.target_id, self.target_start, \
            self.target_end,self.target_strand = 0, None, 0, 0, None
        self.score = 0
        self.aligned = ""
        self.alignedQuery = ""
        self.alignedTarget = ""
        self.coordinate_mapped = False
        self.tseq = False
        self.qseq = False
        
        self.tPosToIndex = None
        self.qPosToIndex = None

        self.tPosToIndex = None
        self.qPosToIndex = None



    def _convertCoordinates (self):
        if self.coordinate_mapped:
            return
        if self.target_strand == 1:
            #print >>sys.stderr, "inverting"
            self.revcomp()
        #else:
        #    print >>sys.stderr, "not inverting", self.query_strand
        self._getSeqs ()

        #self.tPosToIndex = np.zeros(len(self.tseq), dtype=int)
        #self.qPosToIndex = np.zeros(len(self.qseq), dtype=int)
        
        #self.tIndexToPos = np.zeros(len(self.alignedTarget), dtype=int)
        #self.qIndexToPos = np.zeros(len(self.alignedQuery), dtype=int)

        #self._cc_helper()
        n = len(self.alignedTarget)
        t_seqlen, q_seqlen = len(self.tseq), len(self.qseq)
        self.tIndexToPos, self.qIndexToPos, self.tPosToIndex, self.qPosToIndex = ahcython.convert_coordinates_helper(self.alignedTarget, self.alignedQuery, int(n), int(t_seqlen), int(q_seqlen))
        self.coordinate_mapped = True

    def _cc_helper(self):
        qPos = 0
        tPos = 0
        for i in range (len(self.alignedTarget)):
            self.tIndexToPos[i] = tPos
            self.qIndexToPos[i] = qPos
            self.tPosToIndex[tPos] = i
            self.qPosToIndex[qPos] = i
            if self.alignedTarget[i] != "-":
                tPos += 1
            if self.alignedQuery[i] != "-":
                qPos += 1
        #print >>sys.stderr, len(self.alignedTarget), tPos, len(self.tseq), qPos, len(self.qseq)

    def _cc_helper2(self):
        qPos = 0
        tPos = 0
        t_pos_inc = [0 if x == '-' else 0 for x in self.alignedTarget]
        q_pos_inc = [0 if x == '-' else 0 for x in self.alignedQuery]        
        for i in range (len(self.alignedTarget)):
            self.tIndexToPos[i] = tPos
            self.qIndexToPos[i] = qPos
            self.tPosToIndex[tPos] = i
            self.qPosToIndex[qPos] = i
            tPos += t_pos_inc[i]
            qPos += q_pos_inc[i]


    def _cc_helper3(self):
        qPos = 0
        tPos = 0
        for i, t, q in zip(range(len(self.alignedTarget)), self.alignedTarget, self.alignedQuery):
            self.tIndexToPos[i] = tPos
            self.qIndexToPos[i] = qPos
            self.tPosToIndex[tPos] = i
            self.qPosToIndex[qPos] = i
            tPos += t_pos_inc[i]
            qPos += q_pos_inc[i]

    def _cc_helper4(self):
        qPos = 0
        tPos = 0
        for i, t, q in zip(range(len(self.alignedTarget)), self.alignedTarget, self.alignedQuery):
            self.tIndexToPos[i] = tPos
            self.qIndexToPos[i] = qPos
            self.tPosToIndex[tPos] = i
            self.qPosToIndex[qPos] = i
            tPos += t_pos_inc[i]
            qPos += q_pos_inc[i]
        
    def _getSeqs (self):
        if self.tseq and self.qseq:
            return
        self.tseq = "".join(self.alignedTarget.split("-"))
        self.qseq = "".join(self.alignedQuery.split("-"))

    def getFlanks (self, event_target_start_raw, event_target_end_raw, flank):
        self._convertCoordinates()
        #print >>sys.stderr, self.tPosToIndex
        #print >>sys.stderr, len(self.tseq)

        event_target_start = event_target_start_raw - 1
        event_target_end = event_target_end_raw + 1

        if event_target_start - self.target_start  - flank < 0 or event_target_end-self.target_start+flank >= len(self.tseq):
            return None, None, None, None, None, None

        #print >>sys.stderr, "in get flanks: ", event_target_start, self.target_start, self.target_end

        target_start_index = self.tPosToIndex[event_target_start-self.target_start]
        target_end_index = self.tPosToIndex[event_target_end-self.target_start]
        target_start_pos = self.tIndexToPos[target_start_index]
        target_end_pos = self.tIndexToPos[target_end_index]

        query_start_pos = self.qIndexToPos[target_start_index]
        query_end_pos = self.qIndexToPos[target_end_index]

        tesp = int(target_start_pos-flank)
        teep = int(target_end_pos+flank)

        if tesp >= 0 and teep < len(self.tseq):
            event_tseq = self.tseq[tesp:teep]
            qesp = self.qIndexToPos[self.tPosToIndex[tesp]]
            qeep = self.qIndexToPos[self.tPosToIndex[teep]]

            event_qseq = self.qseq[qesp:qeep]

            # coordinates of the event in revised target seq
            tscoord = target_start_pos - tesp
            tecoord = target_end_pos   - tesp
            # coordinates of the event in the revised query seq
            qscoord = query_start_pos - qesp
            qecoord = query_end_pos - qesp

            return event_tseq, event_qseq, tscoord, tecoord, qscoord, qecoord
        return None, None, None, None, None, None
        
    def get_edits_from_interval ( self , event_target_start, event_target_end ):
        self._convertCoordinates()

        target_start_index = self.tPosToIndex[event_target_start-self.target_start]
        target_end_index = self.tPosToIndex[event_target_end-self.target_start]
        return self.get_edits_between_indices(target_start_index, target_end_index)


    def get_edits_between_indices ( self, sindex, eindex):
        m_count, mm_count, ins_count, del_count = 0,0,0,0
        for i in range (sindex, eindex):
            if self.alignedTarget[i] == self.alignedQuery[i]:
                m_count += 1
            elif self.alignedTarget[i] == "-":
                del_count += 1
            elif  self.alignedQuery[i] == "-":
                ins_count += 1
            else:
                mm_count += 1
        return m_count, mm_count, ins_count, del_count


    def __str__( self ):
        return 'agar: %d %d %s %d %d %s %s %d %d %s %d' % \
            ( self.query_length, self.target_length, \
              self.query_id, self.query_start, self.query_end, \
              self.query_strand, self.target_id, self.target_start, \
              self.target_end, self.target_strand, self.score )

    def __cmp__( self , other):
        if self.target_id < other.target_id:
            return -1
        elif self.target_id > other.target_id:
            return 1
        elif self.target_start < other.target_start:
            return -1
        elif self.target_start > other.target_start:
            return 1
        else:
            return 0
        
    def revcomp( self ):
        self.aligned = self.aligned[::-1]
        self.alignedQuery = rc(self.alignedQuery)
        self.alignedTarget = rc(self.alignedTarget)
        if self.target_strand == 1:
            self.target_strand = 0
        else:
            self.target_strand = 1
        if self.query_strand == 1:
            self.query_strand = 0
        else:
            self.query_strand = 1
        
    
    def targetPosToIndex (self, pos):
        curr_pos = self.target_start
        target = self.alignedTarget
        if self.target_strand == 1:
            target = target[::-1]
        index = 0
        while curr_pos < pos:
            if target[index] != "-":
                curr_pos += 1
            index += 1
        return index


    def targetPosToIndices (self, spos, epos):
        curr_pos = self.target_start
        target = self.alignedTarget
        if self.target_strand == 1:
            target = target[::-1]
        index = 0
        sindex = None
        eindex = 0
        while curr_pos < epos and index < len(target):
            if curr_pos == spos:
                sindex = index
            if target[index] != "-":
                curr_pos += 1
            index += 1
        if sindex == None:
            return 0,0
        return sindex, index


    def getUpstreamAnchor (self, x, anchor):
        index_e = x
        index_s = x-anchor
        errors = self.aligned[index_s:index_e].count("*")
        enderror = 0
        while index_s >= 0:
            if errors == 0:
                return index_s
            if self.aligned[index_e-1] == "*":
                errors += -1
            index_s += -1
            index_e += -1
            if self.aligned[index_s] == "*":
                errors += 1

        return -1


    def getDownstreamAnchor (self, x, anchor):
        index_s = x
        index_e = x+anchor
        errors = self.aligned[index_s:index_e].count("*")
        enderror = 0
        while index_e < len(self.aligned):
            if errors == 0:
                return index_e
            if self.aligned[index_s] == "*":
                errors += -1
            index_s += 1
            index_e += 1
            if self.aligned[index_e-1] == "*":
                errors += 1

        return -1
            
        

    def getBetweenHistDist (self, hit):
        qdist, tdist = -sys.maxint-1, -sys.maxint-1
        if hit.target_id != self.target_id:
            #return -sys.maxint-1, -sys.maxint-1, -sys.maxint-1, -sys.maxint-1
            return qdist, tdist
        #qs_min, qs_max, ts_min, ts_max = -1, -1, -1, -1
        if (hit.query_start < self.query_start and 
            hit.query_end < self.query_end):
            qdist = self.query_start-hit.query_end
            tdist = self.target_start-hit.target_end
        elif (self.query_start < hit.query_start and 
              self.query_end < hit.query_end):
            qdist = hit.query_start-self.query_end
            tdist = hit.target_start-self.target_end
            
        return qdist, tdist

        
        
        
        
