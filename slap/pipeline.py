#!/usr/env python
from _util import tmpl_bash,create_dir
from shutil import copyfile
from os.path import dirname,isfile
from os import listdir

SPLIT_DIR = "split"
INTERVAL_DIR = "intervals"
ASSEMBLY_DIR = "assembly"

def partition_reads(slap,chrom):
    outdir = "%s/%s/%s" % (slap.workdir,SPLIT_DIR,chrom)
    bashfn = "%s/partition_reads.sh" % outdir
    swap = { "vcf_fn": slap.vcf_fn,
             "bam_fn": slap.bam_fn,
             "outdir": outdir,
             "chrom": chrom,
             "sample": slap.sample,
             "threads": slap.threads_split,
             "coverage": slap.coverage
             }
    tmpl_bash("partition_reads",swap,bashfn)
    return bashfn

def split_window(start,end,window,padding):
    intervals = []
    # List of list of regions with original region split into ideal region sizes
    intervals_chunks_ = range(start,end,window)
    intervals_chunks = []
    for x,y in zip(intervals_chunks_[:-1],intervals_chunks_[1:]):
        intervals_chunks.append([x,y])
    if intervals_chunks[-1][-1] != end:
        intervals_chunks.append([intervals_chunks[-1][-1],end])
    # If the last intervals is small added it to the second to last interval
    # and remove the last interval
    if (intervals_chunks[-1][-1] - intervals_chunks[-1][0]) < (window/5):
        intervals_chunks[-2][-1] = intervals_chunks[-1][-1]
        intervals_chunks = intervals_chunks[:-1]
    # Add padding to the end of the first interval
    intervals.append((intervals_chunks[0][0],intervals_chunks[0][-1] + padding,intervals_chunks[0][0],intervals_chunks[0][-1]))
    # Loop through the intervals except for the last and first
    for interval_chunk in intervals_chunks[1:-1]:
        intervals.append((interval_chunk[0] - padding,interval_chunk[-1] + padding,interval_chunk[0],interval_chunk[-1]))
    # Add padding to the start of the last interval
    intervals.append((intervals_chunks[-1][0] - padding,intervals_chunks[-1][-1],intervals_chunks[-1][0],intervals_chunks[-1][-1]))
    return intervals

def assemble_intervals(slap,chrom,hap,intervals):
    bashfns = []
    for interval in intervals:
        start,end = interval
        # ssemble_interval.sh  assemble_interval_pipeline.sh 
        outdir = "%s/%s/%s/%s/%s/%s_%s" % (slap.workdir,ASSEMBLY_DIR,chrom,hap,INTERVAL_DIR,start,end)
        create_dir(outdir)
        copyfile("%s/tmpl/assemble_interval.sh" % dirname(__file__),"%s/assemble_interval.sh" % outdir)
        bashfn = "%s/assemble_interval_pipeline.sh" % outdir
        split_dir = "%s/%s" % (slap.workdir,SPLIT_DIR)
        bam_fn = "%s/%s/%s.sorted.bam" % (split_dir,chrom,hap)
        unpartioned_bam = "%s/%s/snpdesert.sorted.bam" % (split_dir,chrom)
        file_check = "%s/interval.done" % outdir
        if isfile(file_check):
            continue
        swap = { 'command_': "assembly",
                 'outdir': outdir,
                 'bam_fn': bam_fn,
                 'chrom': chrom,
                 'start': start,
                 'end': end,
                 'ref_fn': slap.ref,
                 'genome_size': end - start,
                 'threads': slap.threads_contigs,
                 'memory': slap.mem_contigs,
                 'unpartioned_bam_fn': unpartioned_bam,
                 'subreads_xml': slap.xml,
                 'chemistry': slap.chemistry,
                 'trim_qual': slap.trim_qual
                 }
        tmpl_bash("assemble_interval_pipeline",swap,bashfn)
        bashfns.append(bashfn)
    return bashfns

def assembly_stitching(slap,chrom,hap):
    outdir = "%s/%s/%s/%s" % (slap.workdir,ASSEMBLY_DIR,chrom,hap)
    bashfn = "%s/assemble_chrom.sh" % outdir
    swap = { 'outdir': outdir,
             'ref': slap.ref,
             'threads': slap.threads_contigs,
             'xml': slap.xml,
             'reads_fofn': slap.reads_fofn,
             'chemistry': slap.chemistry,
             'trim_qual': slap.trim_qual
             }
    tmpl_bash("assemble_chrom",swap,bashfn)
    return bashfn

def msa_regions(slap,chrom):
    outdir = "%s/%s/%s" % (slap.workdir,ASSEMBLY_DIR,chrom)
    bashfn = "%s/msa_regions.sh" % outdir
    swap = { 'outdir': outdir,
             'ref': slap.ref
             }
    tmpl_bash("msa_regions",swap,bashfn)
    return bashfn

def msa(slap,chrom):
    outdir = "%s/%s/%s" % (slap.workdir,ASSEMBLY_DIR,chrom)
    outdir_fa = "%s/%s/%s/msa/fa" % (slap.workdir,ASSEMBLY_DIR,chrom)
    outdir_bash = "%s/%s/%s/msa/bash" % (slap.workdir,ASSEMBLY_DIR,chrom)
    outdir_clu = "%s/%s/%s/msa/clu" % (slap.workdir,ASSEMBLY_DIR,chrom)
    outdir_bed = "%s/%s/%s/msa/variants" % (slap.workdir,ASSEMBLY_DIR,chrom)
    create_dir(outdir_bash)
    create_dir(outdir_fa)
    create_dir(outdir_clu)
    create_dir(outdir_bed)
    fas = listdir(outdir_fa)
    intervals = [fn.split(".")[0] for fn in fas]
    bash_fns = []
    for interval in intervals:
        fafn = "%s/%s.fa" % (outdir_fa,interval)
        clufn = "%s/%s.clu" % (outdir_clu,interval)
        bashfn = "%s/%s.sh" % (outdir_bash,interval)
        outfn_bed = "%s/%s.bed" % (outdir_bed,interval)
        start,end = interval.split('_')
        if isfile(outfn_bed):
            continue
        swap = { 'outfn_fa': fafn,
                 'outfn_clu': clufn,
                 'chrom': chrom,
                 'start': start,
                 'end': end,
                 'outfn_bed': outfn_bed
                 }
        tmpl_bash("call_sv",swap,bashfn)
        bash_fns.append(bashfn)
    return bash_fns
