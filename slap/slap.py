#!/usr/env python
from copy import copy
import sys
from os.path import abspath,isfile
from _util import setup_chromosomes, submit_bashfile, create_dir
from pipeline import partition_reads, split_window, assemble_intervals, assembly_stitching, msa_regions, msa
from io.FastaIO import FastaWriter, FastaReader
import glob

SPLIT_DIR = "split"
ASSEMBLY_DIR = "assembly"
HAPS = ["hap1", "hap2"]

MEM_SPLIT = 8
THREADS_SPLIT = 1
JOBS_SPLIT = 1
WALLTIME_SPLIT = 1
QUEUE_SPLIT = "private"

MEM_CONTIGS = 8
THREADS_CONTIGS = 1
JOBS_CONTIGS = 1
WALLTIME_CONTIGS = 1
QUEUE_CONTIGS = "private"

COVERAGE = 1
WINDOW = 250000
PADDING = 10000
MIN_WINDOW = 5000
TRIM_QUAL = 30

class Slap:
    def __init__(self):
        self.vcf_fn = None
        self.bam_fn = None
        self.sample = None 
        self.workdir = None 
        self.xml = None
        self.reads_fofn = None
        self.ref = None
        self.chrom = None
        self.local = False
        self.only_split = False
        self.only_assemble = False
        self.only_detect_svs = False
        self.mem_split = MEM_SPLIT
        self.num_jobs_split = JOBS_SPLIT
        self.threads_split = THREADS_SPLIT
        self.walltime_split = WALLTIME_SPLIT
        self.queue_split = QUEUE_SPLIT
        self.coverage = COVERAGE
        self.haps = HAPS
        self.window = WINDOW
        self.padding = PADDING
        self.min_window = MIN_WINDOW
        self.mem_contigs = MEM_CONTIGS
        self.num_jobs_contigs = JOBS_CONTIGS
        self.threads_contigs = THREADS_CONTIGS
        self.walltime_contigs = WALLTIME_CONTIGS
        self.queue_contigs = QUEUE_CONTIGS
        self.chemistry = None
        self.trim_qual = TRIM_QUAL

    opts = [("chrom","chrom"),
            ("local","local"),
            ("window","window"),
            ("min_window","min_window"),
            ("threads_split","threads_split"),
            ("mem_split","mem_split"),
            ("jobs_split","num_jobs_split"),
            ("threads_split","threads_split"),
            ("walltime_split","walltime_split"),
            ("threads_contigs","threads_contigs"),
            ("mem_contigs","mem_contigs"),
            ("jobs_contigs","num_jobs_contigs"),
            ("threads_contigs","threads_contigs"),
            ("walltime_contigs","walltime_contigs"),
            ("coverage","coverage"),
            ("chemistry","chemistry"),
            ("only_split","only_split"),
            ("only_assemble","only_assemble"),
            ("only_detect_svs","only_detect_svs"),
            ("trim_qual","trim_qual")]

    def set_option(self,name,val):
        if val or val == 0:
            setattr(self,name,val)

    @classmethod
    def fromoptions(cls,args,options):
        res = cls.fromargs(args)
        for opt in cls.opts:
            command,option = opt
            res.set_option(option,getattr(options,command))
        if res.chrom == None:
            res.chrom = setup_chromosomes(res.ref)
        else:
            res.chrom = res.chrom.split(",")
        return res

    @classmethod
    def fromargs(cls,args):
        res = cls()
        res.bam_fn = "%s" % abspath(args[0])
        res.vcf_fn = "%s" % abspath(args[1])
        res.sample = args[2]
        res.xml = "%s" % abspath(args[3])
        res.reads_fofn = "%s" % abspath(args[4])
        res.ref = "%s" % abspath(args[5])
        res.workdir = "%s" % abspath(args[6])
        return res
    
    def already_split(self,dirname):
        check_file = "%s/done.splitting" % dirname
        if not isfile(check_file):
            return False
        return True

    def run_split(self):
        self.split_dir = "%s/%s" % (self.workdir,SPLIT_DIR)
        bashfns = []
        for chrom in self.chrom:
            dirname = "%s/%s" % (self.split_dir,chrom)
            if self.already_split(dirname):
               print "Already split--check %s..." % dirname
               continue
            print "\tPartitioning %s into haplotypes..." % chrom
            create_dir(dirname)
            bashfn = partition_reads(copy(self),chrom)
            bashfns.append(bashfn)
        submit_bashfile(copy(self),bashfns,"split")

    def run_interval_assembly(self):
        dirname = "%s/%s" % (self.workdir,ASSEMBLY_DIR)
        create_dir(dirname)
        bashfns = []
        for chrom in self.chrom:
            for hap in self.haps:
                dirname = "%s/%s/%s" % (self.workdir,SPLIT_DIR,chrom)
                interval_fn = "%s/%s.sorted.bed" % (dirname,hap)
                if not isfile(interval_fn):
                    continue
                intervals = []
                with open(interval_fn,'r') as f:
                    for line in f:
                        line = line.rstrip().split('\t')
                        chrom = line[0]
                        start = int(line[1])
                        end = int(line[2])
                        if (end - start) > self.window:
                            split_intervals = split_window(start,end,self.window,self.padding)
                            for start,end,assembly_start,assembly_end in split_intervals:
                                intervals.append((start,end))
                        elif (end - start) > self.min_window:
                            intervals.append((start,end))
                bashfns += assemble_intervals(copy(self),chrom,hap,intervals)
        submit_bashfile(copy(self),bashfns,"create_contigs")

    def run_assembly_stitching(self):
        bash_fns = []
        for chrom in self.chrom:
            for hap in self.haps:
                outdir = "%s/%s/%s/%s" % (self.workdir,ASSEMBLY_DIR,chrom,hap)
                file_check = "%s/assemble_chrom.done" % outdir
                if isfile(file_check):
                    continue
                bash_fns.append(assembly_stitching(copy(self),chrom,hap))
        submit_bashfile(copy(self),bash_fns,"stitch_contigs")

    def run_detect_svs(self):
        bash_fns = []
        for chrom in self.chrom:
            bash_fns.append(msa_regions(copy(self),chrom))
        run_local = self.local
        self.local = True
        submit_bashfile(copy(self),bash_fns,"stitch_contigs")
        self.local = run_local
        bash_fns = []
        for chrom in self.chrom:
            fns = msa(copy(self),chrom)
            for fn in fns:
                bash_fns.append(fn)
        submit_bashfile(copy(self),bash_fns,"stitch_contigs")

    def run_finalizing(self):
        for hap in self.haps:
            hap_fn = "%s/%s.fasta" % (self.workdir,hap)
            with FastaWriter(hap_fn) as writer:
                for chrom in self.chrom:
                    fn = "%s/%s/%s/%s/trimmed_quivered_contigs.fasta" % (self.workdir,ASSEMBLY_DIR,chrom,hap)
                    r = FastaReader(fn)
                    for i,record in enumerate(r):
                        writer.writeRecord("%s_%s_%s_%s" % (chrom,hap,i,record.name),record.sequence)
        svs_fn = "%s/svs.bed" % self.workdir
        with open(svs_fn,'w') as svs_fh:
            for chrom in self.chrom:
                chrom_sv_fns = glob.glob("%s/%s/%s/msa/variants/*bed" % (self.workdir,ASSEMBLY_DIR,chrom))
                for chrom_sv_fn in chrom_sv_fns:
                    with open(chrom_sv_fn,'r') as chrom_sv_fh:
                        svs_fh.write(chrom_sv_fh.read())

    def run(self):
        # Start of pipeline
        print "Partitioning chromosomes..."
        self.run_split()
        print "Assembling chromosomes..."
        self.run_interval_assembly()
        print "Stitching chromosomes..."
        self.run_assembly_stitching()
        print "Detecting structural variants..."
        self.run_detect_svs()
        print "Finalizing pipeline..."
        self.run_finalizing()

    def __call__(self):
        run = True
        if self.only_split:
            self.run_split()
            run = False
        if self.only_assemble:
            self.run_interval_assembly()
            self.run_assembly_stitching()
            run = False
        if self.only_detect_svs:
            self.run_detect_svs()
            run = False
        if run:
            self.run()

def parse_args(args):
    from optparse import OptionParser, OptionGroup
    usage = "\n"\
        "%prog [OPTIONS] BAM VCF SAMPLE READS_XML READS_FOFN REF OUTDIR"
    description = \
        " SLAP (Structual variants in Long And Phased genome) assembles a diploid genome" \
        " using SMRT reads with phased SNPs and calls structural" \
        " variants."
    parser = OptionParser(usage=usage,description=description)
    parser.add_option("--chrom",type=str,
                      help="Chomosomes to assemble in comma seperate format")
    parser.add_option("--local",action='store_true',
                      default=False, help="Run locally")
    parser.add_option("--mem_split",type=int,
                      help="Memory for partitioning. (default = 4)")
    parser.add_option("--threads_split",type=int,
                      help="Threads for partitioning. (default = 1)")
    parser.add_option("--jobs_split",type=int,
                      help="Instead of submitting 1 interval as a job in the cluster, submit this many"
                      " as a job. (default = 1)")
    parser.add_option("--walltime_split",type=int,
                      help=" Maximum number of time for each partitioning job to spend in the cluster"
                      " (default = 24)")
    parser.add_option("--queue_split",type=str,
                      help="Queue in the cluster for partitioning"
                      " (default = low)")
    parser.add_option("--coverage",type=int,
                      help="Coverage threshold for assembling region."
                      " (default = 1)")
    parser.add_option("--mem_contigs",type=int,
                      help="Memory for canu/miniasm. Minimum neeed is 8. (default = 8)")
    parser.add_option("--threads_contigs",type=int,
                      help="Threads for canu/miniasm. (default = 1)")
    parser.add_option("--jobs_contigs",type=int,
                      help="Instead of submitting 1 interval as a job in the cluster, submit this many"
                      " as a job. (default = 50)")
    parser.add_option("--walltime_contigs",type=int,
                      help=" Maximum number of time for Canu/miniasm to spend in the cluster"
                      " (default = 24)")
    parser.add_option("--queue_contigs",type=str,
                      help=" Queue in the cluster for Canu/miniasm"
                      " (default = low)")
    parser.add_option("--trim_qual",type=int,
                      help="Minimum quality for trimming"
                      " (default = 30)")
    parser.add_option("--chemistry",type=str,
                      help=" Chemistry used to generate PacBio raw data."
                      " This can usually be read from the XML file but "
                      " sometimes chemistry info is not there.")
    parser.add_option("--only_split",action='store_true',
                      help="only split bam files into haplotypes (default = False)")
    parser.add_option("--only_assemble",action='store_true',
                      help="only assembly haplotypes (default = False)")
    parser.add_option("--only_detect_svs",action='store_true',
                      help="only detect svs (default = False)")
    parser.add_option("--window",type=int,
                      help="Size of region to assemble (default=250000)")
    parser.add_option("--min_window",type=int,
                      help="Min size of region to assemble (default=5000)")
    options, args = parser.parse_args(args)
    if len(args) != 7:
        parser.error("Error: Need BAM, VCF, SAMPLE, READS_XML, READS_FOFN, REF, OUTDIR")
        parser.print_help()
    return options, args

def main():
    options, args = parse_args(sys.argv[1:])
    slap = Slap.fromoptions(args,options)
    return slap()

if __name__ == '__main__':
    sys.exit(main())


