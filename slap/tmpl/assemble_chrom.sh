#!/bin/bash
set -e -x

cd ${outdir}
ls intervals | while read dir
do
    if [ -s intervals/${dir}/final_quiver.contigs.m5 ]
    then 
	cat intervals/${dir}/final_quiver.contigs.m5 | sed "s/^/${dir}_&/g"
    fi
done > quiver.contigs_to_ref.m5

if [ ! -s quiver.contigs_to_ref.m5 ]
then
    exit 0
fi

if [ ! -s contigs_stitched.fasta.fai ]
then    
    shift_m5_coords.py quiver.contigs_to_ref.m5 ${ref} > quiver.contigs_to_ref_shifted.m5
    alignments_to_tiling.py quiver.contigs_to_ref_shifted.m5 --hap_out_fn contigs_stitched.fasta
    sawriter contigs_stitched.fasta
    samtools faidx contigs_stitched.fasta
fi
mkdir -p contigs
mkdir -p reads_mapped_to_contigs
mkdir -p quivered_contigs
# grep ">" contigs_stitched.fasta | cut -c2- | while read contig_name
# do
#     samtools faidx contigs_stitched.fasta ${contig_name} > contigs/${contig_name}.fa
#     sawriter contigs/${contig_name}.fa
# done
cat ${reads_fofn} | while read fn
do
    name=`echo $fn | tr "/" "\t" | rev | cut -f1 | rev`
    if [ ! -s reads_mapped_to_contigs/${name}.sorted.bam.bai ]
    then
	blasr -nproc ${threads} -bestn 1 ${fn} contigs_stitched.fasta -sa contigs_stitched.fasta.sa -out reads_mapped_to_contigs/${name}.bam -bam
	samtools sort -@ ${threads} reads_mapped_to_contigs/${name}.bam reads_mapped_to_contigs/${name}.sorted
	pbindex reads_mapped_to_contigs/${name}.sorted.bam
	samtools index reads_mapped_to_contigs/${name}.sorted.bam
    fi
done
ls reads_mapped_to_contigs/*.sorted.bam > reads_mapped_to_contigs.fofn
grep ">" contigs_stitched.fasta | cut -c2- | while read contig_name
do
    if [ ${chemistry} != "None" ]
    then
	quiver -j ${threads} --referenceWindow ${contig_name} --parametersSpec ${chemistry} reads_mapped_to_contigs.fofn -r contigs_stitched.fasta -o quivered_contigs/${contig_name}.fasta -o quivered_contigs/${contig_name}.fastq
    else
	quiver -j ${threads} --referenceWindow ${contig_name} reads_mapped_to_contigs.fofn -r contigs_stitched.fasta -o quivered_contigs/${contig_name}.fasta -o quivered_contigs/${contig_name}.fastq
    fi
done
cat quivered_contigs/*.fastq > quivered_contigs.fastq
cutadapt --minimum-length 1 -q ${trim_qual},${trim_qual} -o trimmed_quivered_contigs.fastq quivered_contigs.fastq
seqtk seq -a trimmed_quivered_contigs.fastq > trimmed_quivered_contigs.fasta

#/hpc/users/bashia02/gitrepos/mchaisson7/blasr/alignment/bin
${FAST_BLASR}/blasr \
    -alignContigs \
    -noSplitSubreads \
    -bestn 1 \
    -clipping soft \
    -nproc ${threads} \
    trimmed_quivered_contigs.fastq \
    ${ref} \
    -out trimmed_quivered_contigs.sam -sam
samtools view -Sbh trimmed_quivered_contigs.sam > trimmed_quivered_contigs.bam
samtools sort -@ ${threads} trimmed_quivered_contigs.bam trimmed_quivered_contigs.sorted
samtools index trimmed_quivered_contigs.sorted.bam
rm -f trimmed_quivered_contigs.sam
rm -f trimmed_quivered_contigs.bam
#/hpc/users/bashia02/gitrepos/mchaisson7/blasr/alignment/bin
${FAST_BLASR}/blasr \
    -alignContigs \
    -noSplitSubreads \
    -bestn 1 \
    -clipping soft \
    -nproc ${threads} \
    trimmed_quivered_contigs.fastq \
    ${ref} \
    -out trimmed_quivered_contigs.m5 -m 5
echo "" > assembly_chrom.done