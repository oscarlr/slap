#!/bin/bash
set -x 

bam_fn=$1
chrom=$2
start=$3
end=$4
ref_fn=$5
genome_size=$6
threads=$7
memory=$8
unpartioned_bam_fn=$9
subreads_xml=${10}
chemistry=${11}
trim_qual=${12}

if [ ! -s reads.fasta ]
then
    samtools view ${bam_fn} ${chrom}:${start}-${end} | awk '{OFS="\t"; print ">"$1"\n"$10}' - > reads.fasta
fi

if [ ! -s ref.fa ]
then
    samtools faidx ${ref_fn} ${chrom}:${start}-${end} > ref.fa
fi


if [ ! -s canu/raw.contigs.fasta ]
then
    canu -p raw -d canu contigFilter="2 1000 1.0 1.0 2" corMinCoverage=0 genomeSize=${genome_size} errorRate=0.035 useGrid=0 maxThreads=${threads} maxMemory=${memory} -pacbio-raw reads.fasta
fi

if [ -s canu/raw.contigs.fasta ]
then
    cp canu/raw.contigs.fasta raw.contigs.fasta
else
    cat reads.fasta > reads2.fasta
    samtools view ${unpartioned_bam_fn} ${chrom}:${start}-${end} | awk '{OFS="\t"; print ">"$1"\n"$10}' - >> reads2.fasta
    canu -p raw -d canu2 contigFilter="2 1000 1.0 1.0 2" corMinCoverage=0 genomeSize=${genome_size} errorRate=0.035 useGrid=0 maxThreads=${threads} maxMemory=${memory} -pacbio-raw reads2.fasta
    if [ -s canu2/raw.contigs.fasta ]
    then
	cp canu2/raw.contigs.fasta raw.contigs.fasta
    else
	minimap -Sw5 -L100 -m0 canu/raw.trimmedReads.fasta.gz canu/raw.trimmedReads.fasta.gz | gzip -1 > canu/raw.trimmedReads.paf.gz
	miniasm -c 1 -e 2 -f canu/raw.trimmedReads.fasta.gz canu/raw.trimmedReads.paf.gz > canu/raw.trimmedReads.gfa
	cat canu/raw.trimmedReads.gfa | awk '$1 == "S"' | awk '{ print ">"$2"\n"$3 }' > canu/raw.contigs.fasta
	if [ -s canu/raw.contigs.fasta ]
	then
	    cp canu/raw.contigs.fasta raw.contigs.fasta
	else
	    minimap -Sw5 -L100 -m0 canu2/raw.trimmedReads.fasta.gz canu2/raw.trimmedReads.fasta.gz | gzip -1 > canu2/raw.trimmedReads.paf.gz
	    miniasm -c 1 -e 2 -f canu2/raw.trimmedReads.fasta.gz canu2/raw.trimmedReads.paf.gz > canu2/raw.trimmedReads.gfa
	    cat canu2/raw.trimmedReads.gfa | awk '$1 == "S"' | awk '{ print ">"$2"\n"$3 }' > canu2/raw.contigs.fasta
	    if [ -s canu2/raw.contigs.fasta ]
	    then
		cp canu2/raw.contigs.fasta raw.contigs.fasta
	    else
		echo "No contig assembled"
		exit 0
	    fi
	fi
    fi
fi

if [ ! -s reads.xml ]
then
    qname=`cat reads.fasta | grep ">" | sed 's/>/qname=/g' | tr '\n' ' '`
    dataset filter ${subreads_xml} reads.xml ${qname}
fi

if [ ! -s reads_to_contig.sorted.bam.pbi ]
then
    blasr -nproc ${threads} -bestn 1 reads.xml raw.contigs.fasta -out reads_to_contig.bam -bam
    count=`samtools view -c reads_to_contig.bam`
    if [ "${count}" -eq 0 ]
    then
	exit 0
    fi
    samtools sort reads_to_contig.bam reads_to_contig.sorted
    samtools faidx raw.contigs.fasta
    pbindex reads_to_contig.sorted.bam
fi

if [ ! -s trimmed_quiver.contigs.fasta ]
then
    if [ ${chemistry} != "None" ]
    then
	quiver -j ${threads} --parametersSpec ${chemistry} reads_to_contig.sorted.bam -r raw.contigs.fasta -o quiver.contigs.fasta -o quiver.contigs.fastq
    else
	quiver -j ${threads} reads_to_contig.sorted.bam -r raw.contigs.fasta -o quiver.contigs.fasta -o quiver.contigs.fastq
    fi
    cutadapt --minimum-length 1 -q ${trim_qual},${trim_qual} -o trimmed_quiver.contigs.fastq quiver.contigs.fastq
    if [ -s trimmed_quiver.contigs.fastq ]
    then
	seqtk seq -a trimmed_quiver.contigs.fastq > trimmed_quiver.contigs.fasta
    fi
fi
