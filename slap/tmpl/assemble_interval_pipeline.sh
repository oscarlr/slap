#!/bin/bash
set -e -x

cd ${outdir}
if [ ! -s trimmed_quiver.contigs.fasta ]
then
    sh assemble_interval.sh ${bam_fn} ${chrom} ${start} ${end} ${ref_fn} ${genome_size} ${threads} ${memory} ${unpartioned_bam_fn} ${subreads_xml} ${chemistry} ${trim_qual}
fi

if [ ! -f trimmed_quiver.contigs.fasta ]
then
    echo "Not contig assembled"
    echo "" > interval.done
    exit 0
fi

if [ ! -s trimmed_quiver.contigs.m5 ]
then
    blasr -noSplitSubreads -nproc ${threads} -bestn 1 trimmed_quiver.contigs.fasta ref.fa -out trimmed_quiver.contigs.m5
fi

mkdir -p gaps
echo -e "${chrom}\t${start}\t${end}" > gaps/region.bed
awk '{ print ${chrom}"\t"$8+${start}"\t"$9+${start} }' trimmed_quiver.contigs.m5 | sort -k1,1 -k2,2n | bedtools merge -i stdin > gaps/covered.bed
bedtools subtract -a gaps/region.bed -b gaps/covered.bed > gaps/gaps.bed
cat gaps/gaps.bed | while read chrom gap_start gap_end
do
    size=`awk -v gap_start=${gap_start} -v gap_end=${gap_end} 'BEGIN{print gap_end-gap_start}'`
    if [ "${size}" -gt 5000 ]
    then
        new_start=`awk -v gap_start=${gap_start} 'BEGIN{ print gap_start - 5000}'`
        new_end=`awk -v gap_end=${gap_end} 'BEGIN{ print gap_end + 5000}'`
        if [ ! -f gaps/${new_start}_${new_end}/trimmed_quiver.contigs.fasta ]
        then
            mkdir -p gaps/${new_start}_${new_end}
	    cp assemble_interval.sh gaps/${new_start}_${new_end}/
	    cd gaps/${new_start}_${new_end}
	    sh assemble_interval.sh ${bam_fn} ${chrom} ${new_start} ${new_end} ${ref_fn} ${size} ${threads} ${memory} ${unpartioned_bam_fn} ${subreads_xml} ${chemistry} ${trim_qual}
        fi
    fi
done

num_original_contigs=`cat trimmed_quiver.contigs.fasta | grep ">" | wc -l`
if ls gaps/*/trimmed_quiver.contigs.fasta 1> /dev/null 2>&1
then
    mkdir -p amos
elif [ "${num_original_contigs}" -ge 1 ]
then
    mkdir -p amos
else
    cp trimmed_quiver.contigs.m5 final_quiver.contigs.m5
fi
if [ -d amos ]
then
    line_number=0
    while read line
    do
	line_number=$((line_number+1))
	if [ $line_number -eq 2 ]
	then
	    echo $line | tr '[:lower:]' '[:upper:]'
	else
	    echo $line
	fi
	if [ $line_number -eq 4 ]
	then
	    line_number=0
	fi
    done < trimmed_quiver.contigs.fastq > amos/contigs.fastq
    cat gaps/gaps.bed | while read chrom gap_start gap_end
    do
	new_start=`awk -v gap_start=${gap_start} 'BEGIN{ print gap_start - 5000}'`
	new_end=`awk -v gap_end=${gap_end} 'BEGIN{ print gap_end + 5000}'`
	if [ -s gaps/${new_start}_${new_end}/trimmed_quiver.contigs.fasta ]
	then
	    line_number=0
	    while read line
	    do
		line_number=$((line_number+1))
		if [ $line_number -eq 2 ]
		then
		    echo $line | tr '[:lower:]' '[:upper:]'
		else
		    echo $line
		fi
		if [ $line_number -eq 4 ]
		then
		    line_number=0
		fi
	    done < gaps/${new_start}_${new_end}/trimmed_quiver.contigs.fastq >> amos/contigs.fastq
	fi
    done
    cat trimmed_quiver.contigs.fasta | fasta_formatter -w 0 > amos/contigs.fasta
    cat gaps/*/trimmed_quiver.contigs.fasta | fasta_formatter -w 0 >> amos/contigs.fasta
    cat amos/contigs.fasta | awk '{print (NR%2 == 1) ? ">" ++i : $0}' > amos/rename.contigs.fasta
    cat amos/contigs.fastq | awk '{print (NR%4 == 1) ? ">" ++i : $0}' > amos/rename.contigs.fastq
    convert_fastaqual_fastq.py --conversion_type fastq_to_fastaqual -f amos/rename.contigs.fastq -o amos
    toAmos -s amos/rename.contigs.fasta -q amos/rename.contigs.qual -o amos/contigs.amos.afg
    minimus amos/contigs.amos.afg
    listReadPlacedStatus -S -E amos/contigs.amos.bnk > amos/contigs.amos.singletons
    dumpreads -e -E amos/contigs.amos.singletons amos/contigs.amos.bnk > amos/contigs.amos.singletons.fasta
    cat amos/contigs.amos.fasta | fasta_formatter -w 0 > amos/tmp.contigs.amos.total.fasta
    cat amos/contigs.amos.singletons.fasta | fasta_formatter -w 0 >> amos/tmp.contigs.amos.total.fasta
    cat amos/tmp.contigs.amos.total.fasta | awk '{print (NR%2 == 1) ? ">" ++i : $0}' > amos/contigs.amos.total.fasta  
    blasr -nproc ${threads} -bestn 1 reads.xml amos/contigs.amos.total.fasta -out amos/reads_to_contig.bam -bam
    samtools sort amos/reads_to_contig.bam amos/reads_to_contig.sorted
    samtools faidx amos/contigs.amos.total.fasta
    pbindex amos/reads_to_contig.sorted.bam
    if [ ${chemistry} != "None" ]
    then
	quiver --parametersSpec ${chemistry} amos/reads_to_contig.sorted.bam -j ${threads} -r amos/contigs.amos.total.fasta -o amos/quiver.contigs.fasta -o amos/quiver.contigs.fastq
    else
	quiver amos/reads_to_contig.sorted.bam -j ${threads} -r amos/contigs.amos.total.fasta  -o amos/quiver.contigs.fasta -o amos/quiver.contigs.fastq
    fi
    cp amos/quiver.contigs.fasta final_quiver.contigs.fasta
    cp amos/quiver.contigs.fastq final_quiver.contigs.fastq
    blasr -noSplitSubreads -nproc ${threads} -bestn 1 final_quiver.contigs.fastq ref.fa -out final_quiver.contigs.m5 -m 5
    echo "" > interval.done
fi
