#!/bin/bash

kalign -f clu -s 100 -e 0.85 -t 0.45 -m 0 -in ${outfn_fa} | sed 's/Kalign/CLUSTAL/g' > ${outfn_clu}
msaToVariants.py ${outfn_clu} ${chrom} ${start} ${end} > ${outfn_bed}



