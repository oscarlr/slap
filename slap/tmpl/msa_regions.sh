#!/bin/bash

cd ${outdir}
mkdir -p msa
cat hap1/trimmed_quivered_contigs.m5 | awk '{print $6"\t"$8"\t"$$9"\t"$0}' > msa/hap1_m5.bed
cat hap2/trimmed_quivered_contigs.m5 | awk '{print $6"\t"$8"\t"$$9"\t"$0}' > msa/hap2_m5.bed

bedtools intersect -a msa/hap1_m5.bed -b msa/hap2_m5.bed | cut -f1-3 | sort -k2,2n | bedtools cluster -i stdin -d 1 > msa/hap1_to_hap2_m5.bed
bedtools subtract -a msa/hap1_m5.bed -b msa/hap1_to_hap2_m5.bed | cut -f1-3 | sort -k2,2n | bedtools cluster -i stdin -d 1 > msa/only_hap1_m5.bed
bedtools subtract -a msa/hap2_m5.bed -b msa/hap1_to_hap2_m5.bed | cut -f1-3 | sort -k2,2n | bedtools cluster -i stdin -d 1 > msa/only_hap2_m5.bed

bedM5toOverlapFasta2.py msa/hap1_to_hap2_m5.bed msa/only_hap1_m5.bed msa/only_hap2_m5.bed hap1/trimmed_quivered_contigs.m5 hap2/trimmed_quivered_contigs.m5 ${ref} ${outdir}/msa/fa
