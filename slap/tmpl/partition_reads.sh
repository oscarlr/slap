#!/bin/bash
set -e -x

splitBamByPhasedVcf.py \
    --vcf_fn ${vcf_fn} \
    --bam_fn ${bam_fn} \
    --outdir ${outdir} \
    --chrom ${chrom} \
    --sample ${sample}

if [ -s ${outdir}/hap1.bam ]
then
    samtools sort -@ ${threads} ${outdir}/hap1.bam ${outdir}/hap1.sorted
    samtools index ${outdir}/hap1.sorted.bam
    rm -f ${outdir}/hap1.bam
    coverageBamToBed.py ${outdir}/hap1.sorted.bam ${outdir}/hap1.sorted.bed ${coverage}
fi

if [ -s ${outdir}/hap2.bam ]
then
    samtools sort -@ ${threads} ${outdir}/hap2.bam ${outdir}/hap2.sorted
    samtools index ${outdir}/hap2.sorted.bam
    rm -f ${outdir}/hap2.bam
    coverageBamToBed.py ${outdir}/hap2.sorted.bam ${outdir}/hap2.sorted.bed ${coverage}
fi

if [ -s ${outdir}/snpdesert.bam ]
then
    samtools sort -@ ${threads} ${outdir}/snpdesert.bam ${outdir}/snpdesert.sorted
    samtools index ${outdir}/snpdesert.sorted.bam
    rm -f ${outdir}/snpdesert.bam
    coverageBamToBed.py ${outdir}/snpdesert.sorted.bam ${outdir}/snpdesert.sorted.bed ${coverage}
fi

if [ -s  ${outdir}/snpdesert.sorted.bam.bai ]
then
    echo "" > ${outdir}/done.splitting
fi
