#!/bin/bash

ls -d $PWD/subreads/*bam > subreads.fofn
dataset create --type SubreadSet subreads.xml subreads.fofn
slap --trim_qual 0 --local reads_aligned.sorted.bam snps.vcf.gz 26894 subreads.xml subreads.fofn ref.fa test