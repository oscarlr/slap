#!/bin/bash
module load samtools seqtk amos BEDTools fastx java/1.8.0_66 gcc glib

export PATH=/sc/orga/projects/pacbio/modules/smrtanalysis/3.0.0/smrtsuite/smrtcmds/bin:${PATH}
export PATH=/sc/orga/work/rodrio10/tools/canu-1.4/Linux-amd64/bin:${PATH}
export PATH=/sc/orga/work/rodrio10/tools/miniasm:${PATH}
export PATH=/sc/orga/work/rodrio10/tools/minimap:${PATH}
export PATH=/sc/orga/work/rodrio10/tools/kalign:${PATH}
export FAST_BLASR=/hpc/users/bashia02/gitrepos/mchaisson7/blasr/alignment/bin

ls -d $PWD/subreads/*bam > subreads.fofn
dataset create --type SubreadSet subreads.xml subreads.fofn
slap --trim_qual 0 --local reads_aligned.sorted.bam snps.vcf.gz 26894 subreads.xml subreads.fofn ref.fa test1
